/**
 * Módulo para manejar los eventos
 * @return {function}           [Método que inicializa el módulo]
 */

var standar = (function(){

  // Elementos del DOM
  var DOM = {
    body:                       $('body'),
    header:                     $('header'),
    main:                       $('main'),
    button_menu_mobile:         $('.menu-mobile__button-burger'),
    items_menu_mobile:          $('.menu-mobile__items'),
    form_login :                $('#form-login'),
    menu_toogle :               $('.data-session-on__toogle'),
    select_orden:               $("#select_orden"),
    name_hotel:                 $("#name_hotel")
  };

  var init = function(){
    events.init_dropdown();
    events.focus_input_seacrh();
  };

  var events = {};

  events.focus_input_seacrh = function(){
    DOM.name_hotel.on('focus',function(){
      var $t = $(this);
      var $p = $t.closest(".jsearch__item").addClass('focus');
    }).on('focusout',function() {
      var $t = $(this);
      var val = $t.val();
      if($.trim(val)==''){
        var $p = $t.closest(".jsearch__item").removeClass('focus');
      }
    })
  }

  events.lister_scroll_header_fixed = function(){
    var h = DOM.header.outerHeight();
    DOM.body.css('padding-top',h+'px');    
    $(window).resize(function() { 
      var h = DOM.header.outerHeight();
      DOM.body.css('padding-top',h+'px');
    });
  }
  
  events.init_dropdown = function(){
    DOM.select_orden.ddslick(); 
  } 
  
  events.lister_scroll_header_fixed = function(){
    var h = DOM.header.outerHeight();
    DOM.body.css('padding-top',h+'px');    
    $(window).resize(function() { 
      var h = DOM.header.outerHeight();
      DOM.body.css('padding-top',h+'px');
    });
  }

  events.init_main_banner=function(){

    if(DOM.main_banner){
      DOM.main_banner.slick({
        dots: true,
        infinite: true,
        arrows: true,
        speed: 500,
        fade: true,
        cssEase: 'linear',
        autoplay: true,
        pauseOnHover: false,
        autoplaySpeed: 2000,
      });
    }
  }

 
  return {
    start: init
  };

})();

$(function(){
  standar.start();
});


var valida_ddslick = function(form) {
  var dom = {};
  var events = {};
  var exports = {};

  exports.init = function() {
	  catchDom();
    return events.search();
  };

  var catchDom = function() {
    dom.parent         = $(form);
  };

  events.scrollTop_error =  function(){
    var input_errores = dom.parent.find("input.error,.dd-select--error");
    if (input_errores.length==0) {return 0;}else{
      var ele = input_errores[0];
      var top = $(ele).offset().top;
      var header_h = $("header").outerHeight();
      top = top - header_h - 50;
      $('body, html').animate({
        scrollTop: top+'px'
      }, 500);
      return input_errores.length;
    }
  };

  events.search = function() {
    var dd_container = dom.parent.find(".dd-container");
    $.each(dd_container, function( index, value ) {
      var t = $(this);
      var val = t.find('.dd-selected-value').val();
      if(val == 0){
        t.find('div.dd-select').addClass('dd-select--error');
        t.find('ul.dd-options').addClass('dd-options--error');
        var str = '<label class="Label--error">Debe seleccionar una opción</label>';
        str='';
        if(t.parent().parent().append().find('.Label--error').length>0){

        }else{
          t.parent().parent().append(str);
        }
        
      }
     });

    var num = events.scrollTop_error();
    return num;
  

  };  

  return exports;
};
