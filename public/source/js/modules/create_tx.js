/**
 * Módulo para manejar los eventos disparados por los elementos del Generate Transaction
 * @return {function}           [Método que inicializa el módulo]
 */
var create_tx = (function(){

  // Elementos del DOM
  var DOM = {
    droplist_payment_method: $('#payment_method'),
    input_amount: $('input[name="amount"]'),
    button_login: $('.Button-login'),
    create_form: $('.create_tx_wrapper form'),
    modal_wrapper: $('.modal_wrapper')
  };

  var init = function(){
    events.create_dropdown();
    events.close_modal();
    // events.send_form();
  };


  var events = {};

  events.close_modal=function(){
    var btn = DOM.modal_wrapper.find(".Button.cancel");
    btn.on('click',function(){
      DOM.modal_wrapper.fadeOut('200');
    });
  }

  events.create_dropdown = function(){
    DOM.droplist_payment_method.ddslick({
      onOpen: function(data){
        setTimeout(function () {
          scroll_ayment_method.refresh();
        }, 200);
        
      }
    });  


    var scroll_ayment_method = new IScroll('#payment_method .dd-options', {
			mouseWheel: true,
			scrollbars: true,
			interactiveScrollbars: true,
			click: true
    });

  };

  return {
    start: init
  };

})();

$(function(){
  create_tx.start();
});
