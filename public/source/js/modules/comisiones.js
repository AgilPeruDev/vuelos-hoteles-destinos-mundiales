// Definición de un par de variables
var dataList = document.querySelector('#json-aerolineas'),
    input = document.querySelector('#list-comi-aerolineas');
    botonDescargar =   $('#boton_descargar');
    formPrincipal = $('.wrapper-comisiones');
// Creamos un nuevo XMLHttpRequest
var request = new XMLHttpRequest();
// Una ves listo, definimos eventos para sus estados
request.onreadystatechange = function(response) {
  if (request.readyState === 4) {
    if (request.status === 200) {
      // Parseando JSON
      var jsonOptions = JSON.parse(request.responseText);
      // Iterando sobre el arreglo JSON
      jsonOptions.forEach(function(item) {
        // Creando un option por cada país.
        var option = document.createElement('option');
        option.value = item;
        dataList.appendChild(option);
      });
      input.placeholder = "Escribe el nombre de la aerolínea";
    } else {
      // Error!
      input.placeholder = "Error al cargar lista de aerolíneas ;^(";
    }
  }
};

// Cambiamos el texto del placeholder
input.placeholder = "Escribe el nombre de la aerolínea";
// Abrimos y enviamos el request
request.open('GET', './data/json-aerolineas.json', true);
request.send();

$("#list-comi-aerolineas").change(function(){
    $(".iframe-pdf-comision").removeClass("iframe-pdf-comision--hide");
    $(".opciones-pdf").removeClass("opciones-pdf--hide");
    if ($('#list-comi-aerolineas').val() == "") {
      $(".iframe-pdf-comision").addClass("iframe-pdf-comision--hide");
      $(".opciones-pdf").addClass("opciones-pdf--hide");
    }
});

$("#form-correo").validate({
  success: function(form){
  },
  onSubmit: function(element) {
    $(element).valid();
  },
  submitHandler: function(form) {
    $('#success_comisiones').modal('show');
  }
}); 

$('#list-comi-aerolineas').keydown(function(event) {
  if (event.keyCode === 13) {    
    event.preventDefault();
    var jsonOptions = JSON.parse(request.responseText);
    // Iterando sobre el arreglo JSON
    jsonOptions.forEach(function(item) {
      // Preguntamos si tenemos un valor que estamos escribiendo y que esta en la lista
      if ($('#list-comi-aerolineas').val() == item) {
        $(".iframe-pdf-comision").removeClass("iframe-pdf-comision--hide");
        $(".opciones-pdf").removeClass("opciones-pdf--hide");
      }
    });

  }
  if ($('#list-comi-aerolineas').val() == "") {
    $(".iframe-pdf-comision").addClass("iframe-pdf-comision--hide");
    $(".opciones-pdf").addClass("opciones-pdf--hide");
  }
});


