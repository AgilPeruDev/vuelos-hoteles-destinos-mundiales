// var formDatosValidate = (function(){
//   var events = {};

//   // Elementos del DOM
//   var DOM = {
//     content_message_error: $('.content_message_error'),
//     modal_wrapper: $('.modal_wrapper'),
//     droplist_currency: $('#currency'),
//     icon_currency: $('.icon_currency')
//   };

//   var init = function() {
//     events.validarFormulario('.create_tx_wrapper form',router.create);
//     events.validarFormulario_login('.login_wrapper form',router.login);
//     events.validarFormulario('.first_change_pass_wrapper form',router.first_change);
//     events.validarFormulario('.forgot_wrapper form',router.forgot);
//     events.validarFormulario('.change_pass_wrapper form',router.change_pass);
//     events.resize_screen_load();
//     events.resize_screen();
//   };

//   $.validator.addMethod("validEmail", function (value, element) {
//     return this.optional(element) || /^(?!\.)("([^"\r\\]|\\["\r\\])*"|(([-a-z0-9!#$%&"*+\/=?^_`{|}~]))(\.)?)*@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$/i.test(value);
//   }, "Ingresa un correo válido.");

//   $.validator.addMethod("method_password", function (value, element) {
//     return this.optional(element) || /^[a-zA-Z0-9!@#$_=\\-`.+\"]*$/i.test(value);
//   }, "El password debe ser Alfanumérico.");
  
//   $.validator.addMethod("lettersonly", function(value, element) {
//     return this.optional(element) || /^[a-z]+$/i.test(value);
//   }, "Letters only please"); 
  
//   events.validarFormulario = function(obj_form,parameter) {

//     var form_datos            = $(obj_form);
//     var Input_email           = form_datos.find('.Input_email');
//     var Input_old_password    = form_datos.find('.Input_old_password');
//     var Input_new_password    = form_datos.find('.Input_new_password');
//     var Input_cnew_password   = form_datos.find('.Input_cnew_password');
//     var input_user            = form_datos.find('.Input_user');
//     var input_password        = form_datos.find('.Input_password');
//     var Input_amount          = form_datos.find('.Input_amount');


    
//       // onkeyup: function(element) {
//       //   $(element).valid();
//       // }

      
//     form_datos.validate({
//       success: function(form){
//         events.resize_screen();
//         setTimeout(function(){
//           if(form_datos.find('input.error').length==0){
//             DOM.content_message_error.hide();
//             DOM.content_message_error.html('');
            
//           }
//         },0);
//       },
//       onSubmit: function(element) {
//         $(element).valid();
//       },
//       submitHandler: function(form) {
//         if (parameter instanceof Function) {
//           parameter();
//         }else{
//           window.location.href = parameter;
//         }
//       },
      
//       showErrors: function(errorMap, errorList) {
//         if (errorList.length) {
          
//             var s = errorList.shift();
//             var n = [];
//             n.push(s);
//             var ele = n[0].element;
//             var msg = n[0].message;
//             events.errorPlacementValidation(msg,ele);
//             this.errorList = n;
          
//        }
       
//        this.defaultShowErrors();
//        events.resize_screen();
//       },
//       errorPlacement: function (error, element) {
//         events.resize_screen();
//       }
//     });


//     Input_new_password.mask("AAAAAAAAAAAAAAAA", {
//       'translation': {'A': {pattern: "^[a-zA-Z0-9!@#$_=\\-`.+\"]*$"}}});
//     Input_cnew_password.mask("AAAAAAAAAAAAAAAA", {
//       'translation': {'A': {pattern: "^[a-zA-Z0-9!@#$_=\\-`.+\"]*$"}}});
//     Input_old_password.mask("AAAAAAAAAAAAAAAA", {
//     'translation': {'A': {pattern: "^[a-zA-Z0-9!@#$_=\\-`.+\"]*$"}}});
//     input_password.mask("AAAAAAAAAAAAAAAA", {
//       'translation': {'A': {pattern: "^[a-zA-Z0-9!@#$_=\\-`.+\"]*$"}}});
  
//       var options =  {
//         onComplete: function(cep) {
//         },
//         onKeyPress: function(cep, event, currentField, options){
//         },
//         onChange: function(cep){
//           console.log('cep changed! ', cep.length);

//           var cero = cep.substr(0,1);
//           if(cep.length>5 && cero==0){
            
//             var cepvar = cep.substr(1,6);
//             console.log('cepvar! ', cepvar);
//             Input_amount.val(cepvar);
//           }

//           if(cep.length<6){
//             var cepvar = cep.padStart(5,'0');
//             Input_amount.val(cepvar);
//           }
          
//           if(cep.length==0){
//             Input_amount.val('00.00');
//           }
//         },
//         onInvalid: function(val, e, f, invalid, options){
//           var error = invalid[0];
//           console.log ("Digit: ", error.v, " is invalid for the position: ", error.p, ". We expect something like: ", error.e);
//         },reverse: true
//       };

//     Input_amount.mask("000000000.00", options);
//     Input_amount.on('focus',function(){
//       if($(this).val()=='00.00' || $(this).val()==''){
//         $(this).val("00.00");
//       }
//     });

//     Input_email.rules("add", {
//       required: true,
//       validEmail: true,
//       messages: {
//         required: "Ingresa tu correo electrónico."
//       }
//     });

//     Input_old_password.rules("add", {
//       required: true,
//       messages: {
//         required: "Ingrese su contraseña actual."
//       }
//     });
    
//     Input_new_password.rules("add", {
//       required: true,
//       minlength : 6,
//       maxlength : 16,
//       messages: {
//         required: "Ingrese su nueva contraseña.",      
//         minlength : "La longitud mínima son 6 caracteres.",
//         maxlength : "La longitud máxima son 6 caracteres."
//       }
//     });

//     Input_cnew_password.rules("add", {
//       required: true,
//       equalTo: "#Input_new_password",
//       minlength : 6,
//       maxlength : 16,
//       messages: {
//         required: "Confirma tu contraseña.",
//         equalTo: "La contraseña no coincide.",      
//         minlength : "La longitud mínima son 6 caracteres.",
//         maxlength : "La longitud máxima son 6 caracteres."
//       }
//     });

//     input_user.rules("add", {
//       required: true,
//       messages: {
//         required: "Ingresa tu usuario."
//       }
//     });
    
//     input_password.rules("add", {
//       required: true,
//       minlength : 6,
//       maxlength : 16,
//       messages: {
//         required: "Ingresa tu contraseña.",        
//         minlength : "La longitud mínima son 6 caracteres.",
//         maxlength : "La longitud máxima son 6 caracteres."
//       }
//     });

//     Input_amount.rules("add", {
//       required: true,
//       min: 14,
//       max: 1000,
//       messages: {
//         required: "Debes ingresar un monto.",
//         min: "El monto mínimo permitido es S/ 14.00",
//         max: "El monto máximo permitido es S/ 1000.00"
//       }
//     });


//     if($('#currency').length > 0){
//       DOM.droplist_currency.ddslick({
//         onOpen: function(data){
//           setTimeout(function () {
//             scroll_currency.refresh();
//           }, 200);
//         },
//         defaultSelectedIndex: 1,
//         onSelected: function(data){
          
//           var title = data.selectedData.title;
//           var val = data.selectedData.value;
          
//           if($('.Input_amount').val()!=''){
//             $('.create_tx_wrapper form').valid();
//           }
          
//           if(val=='soles'){
            
//             Input_amount.rules("add", {
//             required: true,
//             min: 14,
//             max: 1000,
//             messages: {
//               required: "Debes ingresar un monto.",
//               min: "El monto mínimo permitido es S/ 14.00",
//               max: "El monto máximo permitido es S/ 1000.00"
//               }
//             });
//           }else{
//             Input_amount.rules("add", {
//               required: true,
//               min: 4,
//               max: 1000,
//               messages: {
//                 required: "Debes ingresar un monto.",
//                 min: "El monto mínimo permitido es USD 4.00",
//                 max: "El monto máximo permitido es USD 1000.00"
//               }
//             });          
//           }
//           DOM.icon_currency.html(title);
//         }
//       });
    
//       var scroll_currency = new IScroll('#currency .dd-options', {
//         mouseWheel: true,
//         scrollbars: true,
//         interactiveScrollbars: true,
//         click: true
//       });
//     }
//   };

//   events.validarFormulario_login = function(obj_form,parameter) {

//     var form_datos            = $(obj_form);
//     var Input_email           = form_datos.find('.Input_email');
//     var Input_old_password    = form_datos.find('.Input_old_password');
//     var Input_new_password    = form_datos.find('.Input_new_password');
//     var Input_cnew_password   = form_datos.find('.Input_cnew_password');
//     var input_user            = form_datos.find('.Input_user');
//     var input_password        = form_datos.find('.Input_password');
//     var Input_amount          = form_datos.find('.Input_amount');


    
//       // onkeyup: function(element) {
//       //   $(element).valid();
//       // }

      
//     form_datos.validate({
//       success: function(form){
//         events.resize_screen();
//         setTimeout(function(){
//           if(form_datos.find('input.error').length==0){
//             DOM.content_message_error.hide();
//             DOM.content_message_error.html('');
            
//           }
//         },0);
//       },
//       onSubmit: function(element) {
//         $(element).valid();
//       },
//       submitHandler: function(form) {
//         if (parameter instanceof Function) {
//           parameter();
//         }else{
//           window.location.href = parameter;
//         }
//       },
      
//       showErrors: function(errorMap, errorList) {
//         console.log("SHOWWW",errorList);

//         if (errorList.length) {
//           var form = $(errorList[0].element).closest('form');
//           var le = form.find('input[type=text]').length + form.find('input[type=password]').length + form.find('input[type=email]').length;
//           if(errorList.length === le && le !=1){
//             DOM.content_message_error.html('<label class="error">Ingresa los datos</label>');
//             DOM.content_message_error.fadeIn(200);
//           }else{
//             var s = errorList.shift();
//             var n = [];
//             n.push(s);
//             var ele = n[0].element;
//             var msg = n[0].message;
//             events.errorPlacementValidation(msg,ele);
//             this.errorList = n;
//           }


//        }
//        this.defaultShowErrors();
//        events.resize_screen();
//       },
//       errorPlacement: function (error, element) {
//         console.log("Placement",error);
//         events.resize_screen();
//         //events.errorPlacementValidation(error, element);
//       }
//     });


//     Input_new_password.mask("AAAAAAAAAAAAAAAA", {
//       'translation': {'A': {pattern: "^[a-zA-Z0-9!@#$_=\\-`.+\"]*$"}}});
//     Input_cnew_password.mask("AAAAAAAAAAAAAAAA", {
//       'translation': {'A': {pattern: "^[a-zA-Z0-9!@#$_=\\-`.+\"]*$"}}});
//     Input_old_password.mask("AAAAAAAAAAAAAAAA", {
//     'translation': {'A': {pattern: "^[a-zA-Z0-9!@#$_=\\-`.+\"]*$"}}});
//     input_password.mask("AAAAAAAAAAAAAAAA", {
//       'translation': {'A': {pattern: "^[a-zA-Z0-9!@#$_=\\-`.+\"]*$"}}});
  
    
//     Input_amount.mask("000000000.00", {reverse: true});

//     Input_email.rules("add", {
//       required: true,
//       validEmail: true,
//       messages: {
//         required: "Ingresa tu correo electrónico."
//       }
//     });

//     Input_old_password.rules("add", {
//       required: true,
//       minlength : 6,
//       maxlength : 16,
//       messages: {
//         required: "Ingrese su contraseña actual.",
//         minlength : "La longitud mínima son 6 caracteres.",
//         maxlength : "La longitud máxima son 6 caracteres."
//       }
//     });
    
//     Input_new_password.rules("add", {
//       required: true,
//       minlength : 6,
//       maxlength : 16,
//       messages: {
//         required: "Ingrese su nueva contraseña.",      
//         minlength : "La longitud mínima son 6 caracteres.",
//         maxlength : "La longitud máxima son 6 caracteres."
//       }
//     });

//     Input_cnew_password.rules("add", {
//       required: true,
//       equalTo: "#Input_new_password",
//       minlength : 6,
//       maxlength : 16,
//       messages: {
//         required: "Confirma tu contraseña.",
//         equalTo: "La contraseña no coincide.",      
//         minlength : "La longitud mínima son 6 caracteres.",
//         maxlength : "La longitud máxima son 6 caracteres."
//       }
//     });

//     input_user.rules("add", {
//       required: true,
//       messages: {
//         required: "Ingresa tu usuario."
//       }
//     });
    
//     input_password.rules("add", {
//       required: true,
//       messages: {
//         required: "Ingresa tu contraseña."
//       }
//     });

//     Input_amount.rules("add", {
//       required: true,
//       min: 14,
//       max: 1000,
//       messages: {
//         required: "Debes ingresar un monto.",
//         min: "El monto mínimo permitido es S/ 14.00",
//         max: "El monto máximo permitido es S/ 1000.00"
//       }
//     });


//     if($('#currency').length > 0){
//       DOM.droplist_currency.ddslick({
//         onOpen: function(data){
//           setTimeout(function () {
//             scroll_currency.refresh();
//           }, 200);
//         },
//         defaultSelectedIndex: 1,
//         onSelected: function(data){
          
//           var title = data.selectedData.title;
//           var val = data.selectedData.value;
          
//           if($('.Input_amount').val()!=''){
//             $('.create_tx_wrapper form').valid();
//           }
          
//           if(val=='soles'){
            
//             Input_amount.rules("add", {
//             required: true,
//             min: 14,
//             max: 1000,
//             messages: {
//               required: "Debes ingresar un monto.",
//               min: "El monto mínimo permitido es S/ 14.00",
//               max: "El monto máximo permitido es S/ 1000.00"
//               }
//             });
//           }else{
//             Input_amount.rules("add", {
//               required: true,
//               min: 4,
//               max: 1000,
//               messages: {
//                 required: "Debes ingresar un monto.",
//                 min: "El monto mínimo permitido es USD 4.00",
//                 max: "El monto máximo permitido es USD 1000.00"
//               }
//             });          
//           }
//           DOM.icon_currency.html(title);
//         }
//       });
    
//       var scroll_currency = new IScroll('#currency .dd-options', {
//         mouseWheel: true,
//         scrollbars: true,
//         interactiveScrollbars: true,
//         click: true
//       });
//     }
//   };

//   events.errorPlacementValidation = function(error, element) {
//     var form = $(element).closest('form');
//     var le = form.find('input[type=text]').length + form.find('input[type=password]').length + form.find('input[type=email]').length;
//     var all_label = error;
    
//     // if(form.find('input.error').length === le && le !=1){
//     //   DOM.content_message_error.html('<label class="error">Ingresa los datos333333333333</label>');
//     //   DOM.content_message_error.fadeIn(200);
//     //   return 0;
//     // }else{
//     //   if(txt != ''){
//         DOM.content_message_error.html(all_label);
//         DOM.content_message_error.fadeIn(200);
//     //   }
//     // }

//     events.resize_screen();
//   };

//   events.show_modal = function() {
//     var m = $('.Input_amount').val();
//     var ic = $('.icon_currency').html();
//     m = parseFloat(m).toFixed(2);
//     var mc = ic+' '+m;
//     parseFloat(mc).toFixed(2);
//     DOM.modal_wrapper.find('.amount p').html(mc);
//     DOM.modal_wrapper.fadeIn(200);
//   };

//   events.redirectLogin = function(){
//       if($('.content_message_success').length > 0){
//         window.location.href='./create_transaction.html';
//       }else{
//         window.location.href='./first_change_password.html';
//       }
//   };

//   events.resize_screen = function(){
//     var hwin = $( window ).height();
//     var wwin = $( window ).width();
   
//     if(wwin>840){
//       var h = $('.header').outerHeight();
//     }else{
//       var h = 0;
//     }
// 		var m = function(){ return $('.menu').length > 0 ? $('.menu').outerHeight(): 0;}
// 		var w = $('main.wrapper').outerHeight();
//     var sum = h+m()+w + 100;
//     //console.log(h,m(),w,sum,hwin);
// 		if(sum > hwin){
// 			$('footer').removeClass('fixed');
// 		}else{
// 			$('footer').addClass('fixed');
// 		}
//   };

//   events.resize_screen_load = function(){
//     $(window).resize(function(){
//       events.resize_screen();
//     });
//   }
  
//   var router = {
//     'login': events.redirectLogin,
//     'create':events.show_modal,
//     'forgot':'./success_forgot.html',
//     'change_pass':'./success_change_first_password.html',
//     'first_change':'./success_change_first_password.html'
//   };

//   return {
//     start: init
//   };
// })();

// $(function(){
//   formDatosValidate.start();
// });

// Instantiate the Bootstrap carousel



$("#main-banner").slick({
    dots: true,
    infinite: true,
    arrows: true,
    speed: 2000,
    fade: true,
    cssEase: 'linear',
    autoplay: true,
    pauseOnHover: false,
    autoplaySpeed: 1000,
});

  $("#carousel-dm-products").slick({
    dots: true,
    infinite: true,
    slidesToShow: 4,
    autoplay: true,
    autoplaySpeed: 1000,
    slidesToScroll: 1,
    swipeToSlide:true,
    arrows: false,
    responsive: [
        {
          breakpoint: 768,
          settings: {
            arrows: false,
            centerMode: true,
            centerPadding: '40px',
            slidesToShow: 3
          }
        },
        {
          breakpoint: 480,
          settings: {
            arrows: false,
            centerMode: true,
            centerPadding: '40px',
            slidesToShow: 1
          }
        }
      ]
  });  