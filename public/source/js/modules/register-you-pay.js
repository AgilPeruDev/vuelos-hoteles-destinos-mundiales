/**
 * Módulo para manejar los eventos
 * @return {function}           [Método que inicializa el módulo]
 */

var register_you_pay = (function(){

  // Elementos del DOM
  var DOM = {
    form_register_you_pay:      $('#form-register-you-pay'),
    select_num_despositos:       $('#num_despositos'),
    droplist_tipo_operacion_1:    $('#tipo_operacion_1'),
    droplist_tipo_operacion_2:    $('#tipo_operacion_2'),
    droplist_tipo_operacion_3:    $('#tipo_operacion_3'),
    droplist_tipo_operacion_4:    $('#tipo_operacion_4'),
    droplist_tipo_operacion_5:    $('#tipo_operacion_5'),
    droplist_tipo_operacion_6:    $('#tipo_operacion_6'),                    
    droplist_reserva_parthers:  $('#droplist_reserva_parthers'),
    droplist_type_person:       $('.droplist_type_person'), 
    droplist_count_banks:       $('.droplist_count_banks'), 
    droplist_countries:         $('#droplist_countries'),        
    droplist_banks:             $('#droplist_banks'),  
    droplist_tip_doc_credit:    $('#droplist_tip_doc_credit'),         
    droplist_tipo_venta_1:        $('#tipo_venta_1'),    
    droplist_tipo_venta_2:        $('#tipo_venta_2'),    
    droplist_tipo_venta_3:        $('#tipo_venta_3'),    
    droplist_tipo_venta_4:        $('#tipo_venta_4'),    
    droplist_tipo_venta_5:        $('#tipo_venta_5'),    
    droplist_tipo_venta_6:        $('#tipo_venta_6'),                        
    datepicker :                $('.datepicker'),
    method_pay :                $("input[name='method_pay']"),
    periodo :                   $("section.periodo"),
    atach_doc :                 $("section.atach_doc"),
    info_sale :                 $("section.info_sale"),
    section_reserva :           $("section.reserva"),
    section_tarjeta :           $("section.tarjeta"),
    btn_cancel_register_pay :   $("#btn-cancel-register-pay"),
    btn_send_register_pay :     $("#btn-send-register-pay"),
    check_all_item:             $("input[name='all_item']"),
    check_each_item:            $("input[name='each_item']"),
    sections_method_deposit:    $(".method_deposit"),
    sections_method_credit:     $(".method_credit"),
    total_file:                 $("input.total_files"),
    total_size_file:            $("input.total_size_files")    
  };

  var init = function(){
    events.validation_register_you_pay();
    events.init_dropdown();
    events.init_calendar();
    events.choose_method();
    events.val_next_to_atach_doc();
    events.val_next_to_info_sale();
    events.val_next_to_tarjeta();
    events.close_modal_register_pay();  
    events.open_modal_register_pay_success();  
    events.check_all();
    events.add_less_row();
    events.select_currency();
    events.get_count_by_bank();
    events.toogle_add_detail();
    events.add_adjuntos();
    events.upload_input_file();
    events.keyup_amount_detail();
    events.choose_operator();
  };


  var events = {};

  events.choose_operator =  function(){
    DOM.section_tarjeta.find("input[name='operator']").on('change',function(){
      var val = $(this).val();
      if(val=='american'){
        DOM.section_tarjeta.find("input.tarjeta__nro-tarjeta").inputmask({"mask": "9999-9999-9999-999"});
      }
      if(val=='dinner'){
        DOM.section_tarjeta.find("input.tarjeta__nro-tarjeta").inputmask({"mask": "9999-9999-9999-99"});
      }  
      if(val=='mastercard' || val == 'visa'){
        DOM.section_tarjeta.find("input.tarjeta__nro-tarjeta").inputmask({"mask": "9999-9999-9999-9999"});
      }            
    });   
  }

  events.keyup_amount_detail =  function(){
    $(document).on('keyup','input.amount_detail',function(){
      var total_detail = DOM.atach_doc.find(".total_aplica").html();

      total_detail = parseFloat(total_detail);
      var total             = events.calc_total_details();   

      if(total_detail<total){
        var val = $(this).val();
        var lb ="<label class='error exceso'>El monto "+val+" supera el total máximo a pagar</label>";
        $(this).after(lb);
        var val = $(this).val('0.00');
        setTimeout(function(){
          $('.exceso').remove();
        }, 2000);
      }else{
        DOM.info_sale.find(".total_detail").val(total); 
      }
      
    });   
  }

  events.show_all_banks =  function(){
    const list_banks_filter = [];
    const map = new Map();
    for (const item of cuentas_bancos) {
        if(!map.has(item.nameshort)){
            map.set(item.nameshort, true);    // set any value to Map
            list_banks_filter.push({
                nameshort: item.nameshort,
                name: item.name,
                cuentas: item.cuentas,
                moneda: item.moneda,
                img: item.img
            });
        }
    }
    var checks = '';   
    $.each(list_banks_filter, function( index, value ) {
      checks+= '<div class="col-sm-2 check-'+value.nameshort+'">';
      checks+= '<label class="custom-radio-button">';
      checks+= '<input type="radio" name="banks" class="checks_banks" value="'+value.nameshort+'>';
      checks+= '<span class="helping-el"></span>';
      checks+= '<span class="label-text"><img src="./img/'+value.img+'" width="85"></span>';  
      checks+= '</label>';  
      checks+= '</div>';
    }); 
    $('.group_check_banks').html(checks); 
  }

  events.calc_total_details =  function(){

    var num_despositos   = DOM.atach_doc.find("#num_despositos option:selected" ).val();
    var total = 0;

    for (let index = 0; index < num_despositos; index++) {
      var val   = DOM.info_sale.find(".amount_detail:eq("+index+")").val();      
      if(val !==''){
        total = parseFloat(total) + parseFloat(val);
      }
    }
    return total;
  };

  events.sum_size_files= function(size,size_last,numfile_last){
    var total_file          = parseInt(DOM.total_file.val());
    var total_size_file     = parseInt(DOM.total_size_file.val());
    size_last = (size_last!=0)? parseInt(size_last / 1024) : size_last;
    var size_maximo         = DOM.total_size_file.data('size-maximo');
    var size = parseInt(size / 1024);
    var disponible          = size_maximo - total_size_file
    total_size_file         = total_size_file + size - size_last;    
    if(disponible<size){
      return {'result':false,'disponible':disponible};
    }else{      
      total_size_file = DOM.total_size_file.val(total_size_file);
      total_file++;
      total_file      = total_file - numfile_last;
      DOM.total_file.val(total_file);
      return {'result':true};
    }
  }

  events.push_msg_after =  function(self,msg,flag){
    self.parent().parent().find('label.error').remove();

    if(flag){  
      var label     = '<label class="error">'+msg+'</label>';  
      self.parent().parent().append(label);
    }
  }

  events.upload_input_file = function(){
    $(document).on('change','input[type="file"]',function(e){
      var fileName        = e.target.files[0].name;
      var fileNameArr     = fileName.split(".");
      var len             = fileNameArr.length;
      var typeFile        = fileNameArr[len-1];
      typeFile            = typeFile.toUpperCase();
      var formart_allow   = $(this).data('format-allow');
      formart_allow       = formart_allow.toUpperCase();
      var type_allow      = formart_allow.split(",");      

      if(type_allow.includes(typeFile)){
        var size = e.target.files[0].size;
        var size_last = parseInt($(this).attr("data-sizelast"));
        var numfile_last = parseInt($(this).attr("data-numfile"));
        var rpta = events.sum_size_files(size,size_last,numfile_last);        
        if(rpta.result==false){
          var disponible = parseInt(rpta.disponible);
          var msg     = 'El tamaño máximo disponible es de : '+disponible+' KB';
          events.push_msg_after($(this),msg,true);        
          return false;
        }else{
          if(fileName.length>35){
            var congini     = fileName.length - 35;
            fileName        = '...'+fileName.slice(congini, 1000);
          }
          $(this).attr("data-sizelast",size);
          $(this).attr("data-numfile",1);
          $(this).parent().find('span>p').html(fileName);
          events.push_msg_after($(this),msg,false);        
        }
      }else{
        var msg     = $(this).data('message');
        events.push_msg_after($(this),msg,true);
      }
    });
  }  
  
  
  events.add_adjuntos = function(){
    DOM.select_num_despositos.on('change',function(){
      var val         = $(this).val();
      
      for(var j = 0; j< val; j++){
        $(".info_sale__group-adjunto:eq("+j+")").removeClass("info_sale__group-adjunto--hide");
      }
      for(var i = j; i< 6; i++){
        $(".info_sale__group-adjunto:eq("+i+")").addClass("info_sale__group-adjunto--hide");
      }      
      events.apply_mask();      
    });
  }

  
  events.toogle_add_detail = function(){
    $(document).on('click','input.add_detail',function(){
      var gr = $(this).closest('.info_sale__group-adjunto').find('.info_sale__add-detail');
      var val = $(this).val();
      if(val=="si"){
        $(gr).fadeIn(200);
      }else{
        $(gr).fadeOut(200);
      }
    });
  }

  events.get_count_by_bank = function(){
    
    $(document).on('click','.checks_banks',function(){
      var currency = $(this).closest(".info_sale__group-adjunto").find('input.currency:checked').val();
      var self =  $(this);
      if(!is.undefined(currency)){
        var val = $(this).val();
        var list_count = [];
        $.each(cuentas_bancos, function( index, value ) {
         if(value.nameshort == val && value.moneda == currency){
          list_count.push(value);
         }
        });
      }

      var opt ="<option value='0'> Seleccione...";
      $.each(list_count, function( index, value ) {
        opt+="<option value='"+value.cuentas+"'> "+value.cuentas;
      });

      self.closest(".info_sale__group-adjunto").find(".droplist_count_banks").html(opt);

      if(list_count.length == 1){
        self.closest(".info_sale__group-adjunto").find('.droplist_count_banks option:eq(1)').attr('selected', 'selected');
      }

    })
  };

  events.filter_unique_banks_on_check = function(list_banks){
    const list_banks_filter = [];
    const map = new Map();
    for (const item of list_banks) {
        if(!map.has(item.nameshort)){
            map.set(item.nameshort, true);    // set any value to Map
            list_banks_filter.push({
                nameshort: item.nameshort,
                name: item.name,
                cuentas: item.cuentas,
                moneda: item.moneda,
                img: item.img
            });
        }
    }
    var checks = '';

    return list_banks_filter;


    
  };

  events.select_currency= function(){
    DOM.info_sale.find('input.currency').on('change',function(){
      var val = $(this).val();
      var list_banks = [];
      var self =  $(this);
      $.each(cuentas_bancos, function( index, value ) {
        var moneda = value.moneda;
        var nameshort = value.nameshort;
        if($.trim(moneda) == $.trim(val)){
          list_banks.push(value);
        }
      });

      var opt="<option value='0'> Seleccione";
      DOM.droplist_count_banks.html(opt);

      var ll = events.filter_unique_banks_on_check(list_banks);
      console.log(ll);
      
      self.closest(".info_sale__group-adjunto").find("[class*='check-']").hide();

      $.each(ll,function(index,value){
        self.closest(".info_sale__group-adjunto").find('.check-'+value.nameshort).show().find('input[type="radio"]').removeAttr('disabled');
      })
      if(ll.length==1){
        $.each(ll,function(index,value){
          self.closest(".info_sale__group-adjunto").find('.check-'+value.nameshort+' .checks_banks:eq(0)').click();
        })        
      }else{
        $.each(ll,function(index,value){
          self.closest(".info_sale__group-adjunto").find('.checks_banks').removeAttr('checked');
        })          
      }
    });
  }

  events.add_less_row = function(){
    $(document).on('click','a.reserva__add-row',function(e){
      e.preventDefault();
      var row = $(this).closest('tr').clone();
      $(this).closest('tbody').append(row);
      events.apply_mask();
    });
    $(document).on('click','a.reserva__less-row',function(e){
      e.preventDefault();
      var tbody = $(this).closest('tbody');
      if(tbody.find('tr').length>1){
        $(this).closest('tr').remove();
        events.apply_mask();
      }

    });
    
  }
   
  events.calc_total_aplica = function(){
    var inputapl = DOM.atach_doc.find('table td input.aplica:not(:disabled)');
    var val,total=0;
    $.each(inputapl, function( index, value ) {
      val = $.trim($(this).val());
      if(val!='.' && val!=''){
        val = parseFloat($(this).val());
        total = total + val;
      }
    });
    total = total.toFixed(2);

    if(total>0) {
      $("input.amount_detail").removeAttr('disabled');
    }
    else{
      $("input.amount_detail").prop('disabled',true);
    }

    DOM.atach_doc.find('span.total_aplica').html(total);
    //**** si tiene zero en la primera posicion, aplica
    var amount_detail = DOM.info_sale.find('input.amount_detail:eq(0)').val();
    amount_detail = parseInt(amount_detail);
    (amount_detail==0) ? DOM.info_sale.find('input.amount_detail:eq(0)').val(total):'';
    
  };
  events.check_all = function (){
    DOM.check_all_item.on('change', function() {
      if( $(this).is(':checked') ){
        DOM.check_each_item.prop('checked', true);
        $(this).closest('table').find('input.aplica').prop('disabled',false);
        events.calc_total_aplica();
      } else {
        DOM.check_each_item.prop('checked', false);
        $(this).closest('table').find('input.aplica').prop('disabled',true);   
        events.calc_total_aplica();     
      }
    }); 
    DOM.check_each_item.on('change', function() {
      if( $(this).is(':checked') ){
        var inpl = $(this).closest('tr').find('input.aplica');
        inpl.prop('disabled',false);
        events.calc_total_aplica();
      } else {
        $(this).closest('tr').find('input.aplica').prop('disabled',true);
        DOM.check_all_item.prop('checked', false);
        events.calc_total_aplica();
      }
    });   
    

    $(document).on('keyup','input.aplica',function(){
      var val = $(this).val();
      var pendding = $(this).closest('tr').find('div.pendiente').html();
      pendding = parseFloat(pendding);
      if(pendding<val){
        $(this).val(pendding);
        events.calc_total_aplica();
        return false
      }else{
        events.calc_total_aplica();
      }
      
    });    
  };

  events.open_modal_register_pay_success = function(){
    DOM.btn_send_register_pay.on('click',function(e){
      e.preventDefault();
      $('#modal_sure_register_you_pay').modal('hide');  
      setTimeout(function(){
        $('#modal-success-register-you-pay').modal('show');
      }, 500);      
    }); 
  }

  events.close_modal_register_pay = function(){
    DOM.btn_cancel_register_pay.on('click',function(e){
      e.preventDefault();
      $('#modal_sure_register_you_pay').modal('hide');
    }); 
  }
 
  events.val_next_to_info_sale=function(){
    var btn = DOM.atach_doc.find('a.Button');
    btn.on('click',function(e){
      e.preventDefault();
      DOM.info_sale.fadeIn(500);
      $(this).parent().parent().hide();
    })
  }

  events.val_next_to_tarjeta=function(){
    var btn = DOM.section_reserva.find('a.Button.next');
    btn.on('click',function(e){
      e.preventDefault();
      DOM.section_tarjeta.fadeIn(500);
      $(this).parent().parent().hide();
    })
  }  

  events.invertir_fecha =  function(fecha){
    var arrfecha = fecha.split("/");
    return arrfecha[2]+'/'+arrfecha[1]+'/'+arrfecha[0];
  }
  events.val_next_to_atach_doc=function(){
    var btn = DOM.periodo.find('a.Button');
    btn.on('click',function(e){
      e.preventDefault();
      var periodo_ini = DOM.periodo.find("[id='periodo_ini']");
      var periodo_fin = DOM.periodo.find("[id='periodo_fin']");
      
      if(periodo_ini.val()!=='' && periodo_fin.val() !== ''){
        var f_ini = periodo_ini.val();
        var f_fin = periodo_fin.val();
        f_ini = events.invertir_fecha(f_ini);
        f_fin = events.invertir_fecha(f_fin);          
        var per_ini = new Date(f_ini).getTime();
        var per_fin = new Date(f_fin).getTime();
        if(per_ini > per_fin){
          alert("La Fecha Desde debe ser menor a la Fecha Hasta");
        }else{
          DOM.atach_doc.fadeIn(500);
        }              
        //$(this).parent().parent().hide();
      }else{
        if(periodo_ini.val() == ''){
          periodo_ini.addClass("error");
          periodo_ini.focus();
        }
        if(periodo_fin.val() == ''){
          periodo_fin.addClass("error");
          periodo_fin.focus();
        }
      }

    })
  }

  events.choose_method=function(){
    DOM.method_pay.on('change',function(e){
      var val = $(this).val();
      if(val=="deposito"){
        DOM.periodo.fadeIn(500);
        DOM.sections_method_credit.hide();
      }else{      
        DOM.sections_method_deposit.hide();
        DOM.section_reserva.fadeIn(500);
        DOM.section_tarjeta.fadeIn(500);        
        $('#modal-select-method-credit').modal('show');
      }
      $('a.Button.next').parent().parent().show();
    })
  }

  events.get_date_format =  function(years_last){
    var date      = new Date();
    var year      = date.getFullYear();
    var month     = date.getMonth();
    month         = month+1;
    month         = (month<10) ? '0'+month : month;
    var day       = date.getDate();
    day           = (day<10) ? '0'+day : day;
    var hoy       = day+'-'+month+'-'+year;
    var year_last = date.getFullYear()-years_last;
    var startDate = day+'-'+month+'-'+year_last;
    return {'hoy':hoy,'startDate':startDate}

  };
  events.init_calendar = function(){
    var periodo_ini     = DOM.periodo.find("[id='periodo_ini']");      
    var years_last_ini  = periodo_ini.data("years-last");   
    var rptas           = events.get_date_format(years_last_ini);
    var startDate       = rptas.startDate;
    var hoy             = rptas.hoy;

    periodo_ini.datepicker({
      autoclose: true,
      format: 'dd/mm/yyyy',
      language: 'es',
      startDate: startDate,
      endDate: hoy
    }); 
    
  
    var periodo_fin     = DOM.periodo.find("[id='periodo_fin']");   
    var years_last_fin  = periodo_fin.data("years-last");  
    var rptas           = events.get_date_format(years_last_fin);
    var startDate       = rptas.startDate;
    var hoy             = rptas.hoy;    

    periodo_fin.datepicker({
      autoclose: true,
      format: 'dd/mm/yyyy',
      language: 'es',
      startDate: startDate,
      endDate: hoy
    });  
    

    var fecha_in        = DOM.info_sale.find("input.fecha_in");      
    var years_last_in   = fecha_in.data("years-last");   
    var rptas           = events.get_date_format(years_last_in);
    var startDate       = rptas.startDate;
    var hoy             = rptas.hoy;

    fecha_in.datepicker({
      autoclose: true,      
      placeholder: "__-__-__",
      format: 'dd/mm/yyyy',
      language: 'es',
      startDate: startDate,
      endDate: hoy
    });     
    
    
  };
  //disabledDates: ['09-']

  events.apply_mask =  function(){
  
    DOM.atach_doc.find('td>input.aplica').inputmask('Regex', {regex: "^[0-9]{1,6}(\\.\\d{1,2})?$"});
    DOM.info_sale.find('input.amount_detail').inputmask('Regex', {regex: "^[0-9]{1,6}(\\.\\d{1,2})?$"});
    DOM.info_sale.find('input.n_operacion').inputmask({"mask": "9999999999999999"});
    DOM.info_sale.find('input.time_in').inputmask("hh:mm", {
      placeholder: "__:__", 
      insertMode: true,
      hourFormat: 12
    });

    DOM.section_reserva.find("input.input_ticket").inputmask({"mask": "9999999999999"});
    DOM.section_reserva.find("input.monto_tickets").inputmask('Regex', {regex: "^[0-9]{1,6}(\\.\\d{1,2})?$"});
    DOM.section_tarjeta.find("input.n_document_credit").inputmask('Regex', {regex: "[0-9]{1,8}$"});
    DOM.section_tarjeta.find("input.tarjeta__mes-vence").inputmask({"mask": "99"});
    DOM.section_tarjeta.find("input.tarjeta__agno-vence").inputmask({"mask": "99"});    
    DOM.section_tarjeta.find("input.tarjeta__nro-tarjeta").inputmask({"mask": "9999-9999-9999-9999"});
    DOM.section_tarjeta.find("input.tarjeta__ccv").inputmask('Regex', {regex: "[0-9]{1,4}$"});
    DOM.info_sale.find("input.fecha_in").inputmask({"mask": "99/99/9999"});
    DOM.periodo.find("input#periodo_ini").inputmask({"mask": "99/99/9999"});
    DOM.periodo.find("input#periodo_fin").inputmask({"mask": "99/99/9999"});    
  }

  events.validation_register_you_pay = function(){
    DOM.form_register_you_pay.validate({
      success: function(form){
      },
      onSubmit: function(element) {
        $(element).valid();
      },
      onfocusout: function (element) {
            return false;
      },      
      submitHandler: function(form) {
        $('#modal_sure_register_you_pay').modal('show');
      }
    });

    events.apply_mask();

  };  
    

  events.init_dropdown = function(){
    DOM.droplist_tipo_venta_1.ddslick(); 
    DOM.droplist_tipo_venta_2.ddslick(); 
    DOM.droplist_tipo_venta_3.ddslick(); 
    DOM.droplist_tipo_venta_4.ddslick(); 
    DOM.droplist_tipo_venta_5.ddslick(); 
    DOM.droplist_tipo_venta_6.ddslick();                     
    DOM.droplist_tipo_operacion_1.ddslick();
    DOM.droplist_tipo_operacion_2.ddslick();
    DOM.droplist_tipo_operacion_3.ddslick();
    DOM.droplist_tipo_operacion_4.ddslick();
    DOM.droplist_tipo_operacion_5.ddslick();
    DOM.droplist_tipo_operacion_6.ddslick();            
    DOM.droplist_reserva_parthers.ddslick({
      onSelected: function (data) {
        var value = data.selectedData.value;
        if(value!=0){
          DOM.section_reserva.find('input.cod_pnr').removeAttr('disabled');
        } else{
          DOM.section_reserva.find('input.cod_pnr').prop('disabled',true);
        }      
      }
    });  
    DOM.droplist_type_person.ddslick();  
    DOM.droplist_countries.ddslick();    
    DOM.droplist_banks.ddslick(); 
    DOM.droplist_tip_doc_credit.ddslick({
      onSelected: function (data) {
        var value = data.selectedData.value;
        switch (value) {
          case 'dni':
              DOM.section_tarjeta.find("input.n_document_credit").val('');
              DOM.section_tarjeta.find("input.n_document_credit").inputmask('Regex', {regex: "[0-9]{1,8}$"});
            break;
          case 'carnet_extranjeria':
              DOM.section_tarjeta.find("input.n_document_credit").val('');            
              DOM.section_tarjeta.find("input.n_document_credit").inputmask('Regex', {regex: "[A-Za-z0-9]{1,12}$"});
            break;         
          case 'ruc':
              DOM.section_tarjeta.find("input.n_document_credit").val('');            
              DOM.section_tarjeta.find("input.n_document_credit").inputmask('Regex', {regex: "[0-9]{1,11}$"});
            break;
          case 'pasaporte':
              DOM.section_tarjeta.find("input.n_document_credit").val('');            
              DOM.section_tarjeta.find("input.n_document_credit").inputmask('Regex', {regex: "[A-Za-z0-9]{1,13}$"});
            break;
          case 'carnet_ffaa':
              DOM.section_tarjeta.find("input.n_document_credit").val('');            
              DOM.section_tarjeta.find("input.n_document_credit").inputmask('Regex', {regex: "[0-9]{1,12}$"});
            break;
          case 'carnet_policial':
              DOM.section_tarjeta.find("input.n_document_credit").val('');            
              DOM.section_tarjeta.find("input.n_document_credit").inputmask('Regex', {regex: "[0-9]{1,12}$"});
            break; 
          case 'partida_nacimiento':
              DOM.section_tarjeta.find("input.n_document_credit").val('');            
              DOM.section_tarjeta.find("input.n_document_credit").inputmask('Regex', {regex: "[A-Za-z0-9]{1,15}$"});
            break;    
        }     
      }
    });   
    //DOM.droplist_count_banks.ddslick();   
  } 



  return {
    start: init
  };

})();

$(function(){
  register_you_pay.start();
});