/**
 * Módulo para manejar los eventos
 * @return {function}           [Método que inicializa el módulo]
 */

var register = (function(){

  // Instancia de funciones externas
  valida_ddslick = new valida_ddslick("#form-register");

  // Elementos del DOM
  var DOM = {
    form_register :                 $('#form-register'),        
    droplist_agency_country :       $("#agency_country"),
    droplist_agency_city :          $("#agency_city"),
    droplist_agency_district :      $("#agency_district"),
    droplist_user_country :         $("#user_country"),
    droplist_user_city:             $("#user_city"),
    droplist_user_district:         $("#user_district"),    
    droplist_agency_incentive:      $("#agency_incentive"),
    droplist_type_document_agency:  $("#type_document_agency"),
    droplist_type_document_user :   $("#type_document_user"),
    email_user                  :   $("#email_user"),
    datepicker :                    $('.datepicker')    
  };

  var init = function(){
    events.validation_register();
    events.init_dropdown();
    events.init_calendar();
    events.search_errores();
  };

  var events = {};
  var App = {};

 

  events.init_calendar = function(){
    DOM.datepicker.datepicker({
      autoclose: true,
      format: 'dd/mm/yyyy',
      language: 'es',
      endDate: new Date()
    }).on("change", function() {
      $(this).focus().focusout().focus();
    });
  ;
  };
  
  events.scrollTop_error =  function(){
    var input_errores = DOM.form_register.find("input.error");
    var ele = input_errores[0];
    var top = $(ele).offset().top;
    var header_h = $("header").outerHeight();
    top = top - header_h - 50;
    $('body, html').animate({
      scrollTop: top+'px'
    }, 500);

  };
  events.search_errores =  function(){
    DOM.form_register.find("button[type='submit']").on('click',function(){
      valida_ddslick.init();   
    });
  };

  events.validation_register = function(){
    $.validator.addMethod("valida_email", function(value, element) {
      return /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/.test(value);        
    }, "Ingresa un Email válido");

    DOM.form_register.validate({
      success: function(form){
      },
      onSubmit: function(element) {
        $(element).valid();
      },
      submitHandler: function(form) {

        if(valida_ddslick.init()>0){
          return false;
        }else{
          $('#success-register').modal('show');
        }
      }
    });

    
    // Validacion de Agencia
    DOM.form_register.find('input#input_nro_documento_agency').inputmask('Regex', {regex: "[0-9]*"});
    DOM.form_register.find("input#name_agency").inputmask('Regex', {regex: "[a-z&$#.0-9 ]*"});
    DOM.form_register.find("input#address_agency").inputmask('Regex', {regex: "[A-Za-z&#.,0-9 _\-]*"});         

    // Validacion de User
    DOM.form_register.find("input#names").inputmask('Regex', {regex: "[A-Za-z ]*"}); 
    DOM.form_register.find("input#lastname_f").inputmask('Regex', {regex: "[A-Za-z ]*"}); 
    DOM.form_register.find("input#lastname_m").inputmask('Regex', {regex: "[A-Za-z ]*"}); 
    DOM.form_register.find("input#job").inputmask('Regex', {regex: "[A-Za-z&.0-9 _\-]*"});
    DOM.form_register.find("input#address_user").inputmask('Regex', {regex: "[A-Za-z&#.,0-9 _\-]*"});    
    DOM.form_register.find("input#birth_date").inputmask({"mask": "99/99/9999"});  
    DOM.form_register.find('input#number_mobile').inputmask('Regex', {regex: "[0-9]*"});
    DOM.form_register.find("input#franchise").inputmask('Regex', {regex: "[A-Za-z0-9 ]*"});
    DOM.email_user.inputmask('Regex', {regex: "[A-Za-z0-9._@-]{1,100}$"});
    DOM.email_user.rules("add", {valida_email : true });     
    

    // DOM.droplist_type_document_agency.rules('add',{
    //   required : true
    // })  
  };  
    

  events.init_dropdown = function(){

    // var scroll_ayment_method = new IScroll('#agency_country .dd-options', {
		// 	mouseWheel: true,
		// 	scrollbars: true,
		// 	interactiveScrollbars: true,
		// 	click: true
    // });


    DOM.droplist_agency_district.ddslick({
      onSelected: function(data){
        var value = data.selectedData.value;
        if(value!=0){
          var id = data.original.context.id;
          var t = $("#"+id);
          t.find('div.dd-select').removeClass('dd-select--error');
          t.find('ul.dd-options').removeClass('dd-options--error');
          t.parent().parent().find('label.Label--error').remove();          
        }
      }
    }); 

    DOM.droplist_agency_city.ddslick({
      onSelected: function(data){
        var value = data.selectedData.value;
        if(value!=0){
          var id = data.original.context.id;
          var t = $("#"+id);
          t.find('div.dd-select').removeClass('dd-select--error');
          t.find('ul.dd-options').removeClass('dd-options--error');
          t.parent().parent().find('label.Label--error').remove();          
        }
        if(value==0){
          $("#agency_district").ddslick('select', { index: 0 });
         }        
      }      
    }); 

    DOM.droplist_agency_incentive.ddslick({
      onSelected: function(data){
        var value = data.selectedData.value;
        if(value!=0){
          var id = data.original.context.id;
          var t = $("#"+id);
          t.find('div.dd-select').removeClass('dd-select--error');
          t.find('ul.dd-options').removeClass('dd-options--error');
          t.parent().parent().find('label.Label--error').remove();          
        }
      }
    }); 
    DOM.droplist_type_document_agency.ddslick({
      onSelected: function(data){
        var value = data.selectedData.value;
        if(value==0){
          DOM.form_register.find('input#input_nro_documento_agency').attr('disabled','disabled');
          DOM.form_register.find('input#input_nro_documento_agency').removeClass('error');
          DOM.form_register.find('input#input_nro_documento_agency').parent().find('label.error').remove();
        }
        if (value==1){
          DOM.form_register.find('input#input_nro_documento_agency').removeAttr('disabled');
          DOM.form_register.find('input#input_nro_documento_agency').inputmask('Regex', {regex: "[0-9]{11,11}$"}); 
          DOM.form_register.find('input#input_nro_documento_agency').rules("add", {
            minlength: 11,
            messages:{
              minlength: "Ingresa 11 dígitos en el RUC"
            }
          });  
        }
        if (value==2){
          DOM.form_register.find('input#input_nro_documento_agency').removeAttr('disabled');
          DOM.form_register.find('input#input_nro_documento_agency').inputmask('Regex', {regex: "[0-9]{1,11}$"});
          DOM.form_register.find('input#input_nro_documento_agency').rules("add", {
            minlength: 0,
            messages:{
              minlength: " "
            }
          });  
        }
        if(value!=0){
          var id = data.original.context.id;
          var t = $("#"+id);
          t.find('div.dd-select').removeClass('dd-select--error');
          t.find('ul.dd-options').removeClass('dd-options--error');
          t.parent().parent().find('label.Label--error').remove();          
        }
        DOM.form_register.find('input#input_nro_documento_agency').focus().focusout().focus();  
      } 
    }); 
    DOM.droplist_user_district.ddslick({
      onSelected: function(data){
        var value = data.selectedData.value;
        if(value!=0){
          var id = data.original.context.id;
          var t = $("#"+id);
          t.find('div.dd-select').removeClass('dd-select--error');
          t.find('ul.dd-options').removeClass('dd-options--error');
          t.parent().parent().find('label.Label--error').remove();          
        }
      }       
    });  
    DOM.droplist_user_city.ddslick({
      onSelected: function(data){
        var value = data.selectedData.value;
        if(value!=0){
          var id = data.original.context.id;
          var t = $("#"+id);
          t.find('div.dd-select').removeClass('dd-select--error');
          t.find('ul.dd-options').removeClass('dd-options--error');
          t.parent().parent().find('label.Label--error').remove();          
        }

        if(value==0){
          $("#user_district").ddslick('select', { index: 0 });
        }        
      }       
    });
    DOM.droplist_user_country.ddslick({
      onSelected: function(data){
        var value = data.selectedData.value;
        if(value!=0){
          var id = data.original.context.id;
          var t = $("#"+id);
          t.find('div.dd-select').removeClass('dd-select--error');
          t.find('ul.dd-options').removeClass('dd-options--error');
          t.parent().parent().find('label.Label--error').remove();          
        }
        if(value==0){
          $("#user_city").ddslick('select', { index: 0 });
          $("#user_district").ddslick('select', { index: 0 });
        }
      }       
    });    
    DOM.droplist_type_document_user.ddslick({
      onSelected: function(data){
        var value = data.selectedData.value;
        if(value==0){
          DOM.form_register.find('input#input_nro_document_user').attr('disabled','disabled');
          DOM.form_register.find('input#input_nro_document_user').removeClass('error');
          DOM.form_register.find('input#input_nro_document_user').parent().find('label.error').remove();
        }        
        if (value==1){
          DOM.form_register.find('input#input_nro_document_user').inputmask('Regex', {regex: "[0-9]{1,8}$"}); 
          DOM.form_register.find('input#input_nro_document_user').removeAttr('disabled');
          DOM.form_register.find('input#input_nro_document_user').rules("add", {
            minlength: 8,
            messages:{
              minlength: "Ingresa 8 dígitos"
            }
          });          
        }
        if (value==2){
          DOM.form_register.find('input#input_nro_document_user').inputmask('Regex', {regex: "[A-Za-z0-9-]{1,12}$"}); 
          DOM.form_register.find('input#input_nro_document_user').removeAttr('disabled');
          DOM.form_register.find('input#input_nro_document_user').rules("add", {
            minlength: 12,
            messages:{
              minlength: "Ingresa 12 caracteres"
            }
          });           
        }
        if (value==3){
          DOM.form_register.find('input#input_nro_document_user').inputmask('Regex', {regex: "[A-Za-z0-9-]{1,12}$"}); 
          DOM.form_register.find('input#input_nro_document_user').removeAttr('disabled')
          DOM.form_register.find('input#input_nro_document_user').rules("add", {
            minlength: 12,
            messages:{
              minlength: "Ingresa 12 caracteres"
            }
          });            
        }

        if(value!=0){
          var id = data.original.context.id;
          var t = $("#"+id);
          t.find('div.dd-select').removeClass('dd-select--error');
          t.find('ul.dd-options').removeClass('dd-options--error');
          t.parent().parent().find('label.Label--error').remove(); 
          
          DOM.form_register.find('input#input_nro_document_user').focus().focusout().focus();           
        } 
        

      } 
    });    
    
    
    DOM.droplist_agency_country.ddslick({
      onSelected: function(data){
        var value = data.selectedData.value;
        if(value!=0){
          var id = data.original.context.id;
          var t = $("#"+id);
          t.find('div.dd-select').removeClass('dd-select--error');
          t.find('ul.dd-options').removeClass('dd-options--error');
          t.parent().parent().find('label.Label--error').remove();          
        }
        if(value==0){
         $("#agency_city").ddslick('select', { index: 0 });
         $("#agency_district").ddslick('select', { index: 0 });
        }
      }       
      // onOpen: function(data){
      //   setTimeout(function () {
      //     scroll_ayment_method.refresh();
      //   }, 200);
      // }
    });      
  } 



  return {
    start: init
  };

})();

$(function(){
  register.start();
});
