/**
 * Módulo para manejar los eventos
 * @return {function}           [Método que inicializa el módulo]
 */

var register = (function(){

    // Elementos del DOM
    var DOM = {
      form_actualiza_datos: $('#form-lista-datos'),
      datepicker: $('.datepicker'),
      droplist_agency_pais :      $("#actudatos_country"),
      droplist_agency_ciudad :      $("#actudatos_city"),
      droplist_agency_distrito :      $("#actudatos_district"),
    };
  
    var init = function(){
      events.validation_register();
      events.init_calendar();
      events.search_errores();
    };  
  
    var events = {};
  
    events.scrollTop_error =  function(){
      var input_errores = DOM.form_actualiza_datos.find("input.error");
      var ele = input_errores[0];
      var top = $(ele).offset().top;
      var header_h = $("header").outerHeight();
      top = top - header_h - 50;
      $('body, html').animate({
        scrollTop: top+'px'
      }, 500);
   
    };
    events.search_errores =  function(){
      DOM.form_actualiza_datos.find("button[type='submit']").on('click',function(){
        setTimeout(function(){
          events.scrollTop_error();
        }, 10); 
      });
    };

    events.init_calendar = function(){
      DOM.datepicker.datepicker({
        autoclose: true,
        format: 'dd/mm/yyyy',
        language: 'es',
        endDate: new Date()
      });
    };

    events.validation_register = function(){
      DOM.form_actualiza_datos.validate({
        success: function(form){ 
        },
        onSubmit: function(element) {
          $(element).valid();
        },
        submitHandler: function(form) {
          $('#success_actualizacion_datos').modal('show');
        }
      });
      var input_postal_code= DOM.form_actualiza_datos.find("#postal_code");
      if(input_postal_code.length>0) {
        
        input_postal_code.inputmask('Regex', {regex: "[A-Za-z0-9 ]{1,10}$"});
      }

      var input_phone_number_f= DOM.form_actualiza_datos.find("#phone_number_f");
      if(input_phone_number_f.length>0) {
        input_phone_number_f.inputmask('Regex', {regex: "[0-9]{1,50}$"});
      }

      var input_phone_movil_f= DOM.form_actualiza_datos.find("#phone_movil_f");
      if(input_phone_movil_f.length>0) {
        input_phone_movil_f.inputmask('Regex', {regex: "[0-9]{1,50}$"});
      }

      var input_job_phone_f= DOM.form_actualiza_datos.find("#job_phone_f");
      if(input_job_phone_f.length>0) {
        input_job_phone_f.inputmask('Regex', {regex: "[0-9]{1,50}$"});
      }

      var input_annex_phone_f= DOM.form_actualiza_datos.find("#annex_phone_f");
      if(input_annex_phone_f.length>0) {
        input_annex_phone_f.inputmask('Regex', {regex: "[0-9]{1,5}$"});
      }


      var input_name_f= DOM.form_actualiza_datos.find("#name_f");
      if(input_name_f.length>0) {
        input_name_f.inputmask('Regex', {regex: "[A-Za-z]{1,50}$"});
      }

      var input_paternal_lastname_f= DOM.form_actualiza_datos.find("#paternal_lastname_f");
      if(input_paternal_lastname_f.length>0) {
        input_paternal_lastname_f.inputmask('Regex', {regex: "[A-Za-z]{1,50}$"});
      }

      var input_maternal_lastname_f= DOM.form_actualiza_datos.find("#maternal_lastname_f");
      if(input_maternal_lastname_f.length>0) {
        input_maternal_lastname_f.inputmask('Regex', {regex: "[A-Za-z]{1,50}$"});
      }

      var input_direction_f= DOM.form_actualiza_datos.find("#direction_f");
      if(input_direction_f.length>0) {
        input_direction_f.inputmask('Regex', {regex: "[A-Za-z0-9 ]{1,100}$"});
      }

      DOM.droplist_agency_pais.ddslick(); 
      DOM.droplist_agency_ciudad.ddslick(); 
      DOM.droplist_agency_distrito.ddslick();
      
    };  
      
    return {
      start: init
    };
  
  })();
  
  $(function(){
    register.start();
  });
  