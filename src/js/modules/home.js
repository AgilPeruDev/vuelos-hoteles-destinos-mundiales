/**
 * Módulo para manejar los eventos
 * @return {function}           [Método que inicializa el módulo]
 */

var home = (function(){

  // Elementos del DOM
  var DOM = {
    body:                       $('body'),
    header:                     $('header'),
    main:                       $('main'),
    main_banner:                $('#main-banner'),
    carousel_dm_products:       $('#carousel-dm-products'),
    menu_toogle :               $('.data-session-on__toogle'),
    btn_recomendacion:          $('a.recomendaciones')
  };

  var init = function(){
    events.init_main_banner();
    events.init_carousel_dm_products();
    events.show_toogle_recomendaciones();
  };


  var events = {};

  events.show_toogle_recomendaciones = function(){
    DOM.btn_recomendacion.on('click',function(){
      var group   = $(this).data('group');
      var less    = $(this).data('less');
      var more    = $(this).data('more');
      var second  = $(group+':eq(1)');
      if(second.hasClass('wrapper-te-recomendamos__items-row--hide')){
        second.fadeIn(500);
        $(this).html(less);
        second.removeClass('wrapper-te-recomendamos__items-row--hide');
      }else{
        second.fadeOut(500);
        $(this).html(more);
        second.addClass('wrapper-te-recomendamos__items-row--hide');
      }
      
      
    });
  }
  

   
  events.init_main_banner=function(){

    if(DOM.main_banner){
      DOM.main_banner.slick({
        dots: true,
        infinite: true,
        arrows: true,
        speed: 500,
        fade: true,
        cssEase: 'linear',
        autoplay: true,
        pauseOnHover: false,
        autoplaySpeed: 5000,
      });
    }
  }

  events.init_carousel_dm_products=function(){
    DOM.carousel_dm_products.slick({
      dots: true,
      infinite: true,
      slidesToShow: 4,
      autoplay: true,
      autoplaySpeed: 1000,
      slidesToScroll: 1,
      swipeToSlide:true,
      arrows: false,
      responsive: [
          {
            breakpoint: 768,
            settings: {
              arrows: false,
              centerMode: true,
              centerPadding: '40px',
              slidesToShow: 3
            }
          },
          {
            breakpoint: 480,
            settings: {
              arrows: false,
              centerMode: true,
              centerPadding: '40px',
              slidesToShow: 1
            }
          }
        ]
    });    
  }  

 
  return {
    start: init
  };

})();

$(function(){
  home.start();
});
